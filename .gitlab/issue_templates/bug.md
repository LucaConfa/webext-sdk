## Environment

<!-- Please provide the following: -->

* Browser and full version:  
* Extension using this toolkit: test-mv2
* Commit hash: 

<!-- Note: you can get the commit hash by running: git log --pretty=format:'%h' -n 1 -->

## Steps to reproduce

1. (e.g. Load the test extension)
2. (e.g. Run the following code in the background page console)

## Actual behavior

<!-- What behavior do you experience when following the above steps? -->

## Expected behavior

<!-- What behavior do you expect to see instead? -->

/label ~bug