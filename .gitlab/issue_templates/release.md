## Background
<!-- Only in case of hotfix, otherwise, remove this section -->

## Release information

- Version: <!-- Specify release version -->
- Milestone: <!-- Link to the Milestone -->

## Steps

Detailed steps: [Release Process](https://gitlab.com/eyeo/adblockplus/abc/webext-sdk/-/wikis/Release-process) 

- [ ] Update `package.json`
- [ ] Update Release notes
- [ ] Cleanup Milestone issues
- [ ] Tag the version
- [ ] Create next release ticket <!-- Remove in case of the Hotfix -->

/label ~Release
