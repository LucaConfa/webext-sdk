default:
  image: registry.gitlab.com/eyeo/docker/adblockplus-ci:node16-no-python
  interruptible: true

stages:
  - build
  - pages
  - functional
  - fuzz
  - integration

variables:
  npm_config_audit: "false"
  npm_config_fund: "false"
  npm_config_prefer_offline: "true"

cache:
  - key:
      prefix: $CI_JOB_IMAGE
      files:
      - package-lock.json
    paths:
    - node_modules/
    - browser-snapshots/*/cache/

build:
  stage: build
  before_script:
    - npm install
  script:
    - npm run lint
    - npm run build -- --env release
    - DOCS_ERROR=$(npm run docs 2>&1 >/dev/null) && if [ ! -z "$DOCS_ERROR" ]; then echo $DOCS_ERROR && false; fi
    # Non-functional tests run here for convenience
    - npm run test-bundle
    - npm run test-scripts
  artifacts:
    paths:
      - dist/
      - docs/
    exclude:
      - dist/package.json
    expire_in: 1 month

.docker-image:
  image: docker:19.03.5
  services:
    - docker:19.03.5-dind
  variables:
    DOCKER_DRIVER: overlay2

build:integration:
  stage: build
  extends: .docker-image
  script:
    - docker build -t extension -f test/dockerfiles/extension.Dockerfile .
    - docker run extension
    - docker cp $(docker ps -aqf ancestor=extension):/extension .
  artifacts:
    paths:
      - extension
    expire_in: 1 month

audit:
  stage: build
  before_script:
    - npm install
  script:
    - npm audit --production
  allow_failure: true

.functional:
  stage: functional
  extends: .docker-image
  needs: []
  before_script:
    - docker build -t functional -f test/dockerfiles/functional.Dockerfile --build-arg MV3_SUBSCRIPTIONS="$MV3_SUBSCRIPTIONS" .
  script:
    - docker run --shm-size=512m -e TEST_PARAMS="$TEST_PARAMS" functional

func:v2:chromium:stable:
  extends: .functional
  variables:
    TEST_PARAMS: v2 chromium
  
func:v2:chromium:stable:incognito:
  extends: .functional
  variables:
    TEST_PARAMS: v2 chromium --incognito

func:v2-subs:chromium:stable:
  extends: .functional
  variables:
    TEST_PARAMS: v2-custom chromium

func:v2:chromium:oldest:
  extends: .functional
  variables:
    TEST_PARAMS: v2 chromium 77.0.3865.0

func:v2:chromium:beta:
  extends: .functional
  variables:
    TEST_PARAMS: v2 chromium beta

func:v2:chromium:dev:
  extends: .functional
  variables:
    TEST_PARAMS: v2 chromium dev

func:v2:firefox:latest:
  extends: .functional
  variables:
    TEST_PARAMS: v2 firefox

func:v2:firefox:incognito:
  extends: .functional
  variables:
    TEST_PARAMS: v2 firefox --incognito

func:v2:firefox:oldest:
  extends: .functional
  variables:
    TEST_PARAMS: v2 firefox 68.0

func:v2:firefox:beta:
  extends: .functional
  variables:
    TEST_PARAMS: v2 firefox beta

.edge:windows:
  stage: functional
  needs:
    - job: build
      artifacts: true
  variables:
    CI_PROJECT_ID_MAINSTREAM: 22365241
  before_script:
    - Invoke-WebRequest
        -Uri "${Env:CI_API_V4_URL}/projects/${Env:CI_PROJECT_ID_MAINSTREAM}/packages/generic/microsoft-edge/79.0.309/MicrosoftEdgeEnterpriseX64.msi"
        -Headers @{'JOB-TOKEN' = $Env:CI_JOB_TOKEN}
        -OutFile 'MicrosoftEdgeEnterpriseX64.msi'
    - Start-Process msiexec
        -ArgumentList "/i MicrosoftEdgeEnterpriseX64.msi /norestart /qn" -Wait
    - choco upgrade -y nodejs --version 16.10.0
    - npm install -g npm
    - npm install
    - Start-Process "node" -ArgumentList "test/start-server.js" -PassThru
  tags:
    - shared-windows
    - windows
    - windows-1809
  cache: {}

func:v3-default-subs:chromium:stable:
  extends: .functional
  variables:
    TEST_PARAMS: v3 chromium --testKinds functional reload
    MV3_SUBSCRIPTIONS: default

func:v3-test-subs:chromium:stable:
  extends: .functional
  variables:
    TEST_PARAMS: v3 chromium --testKinds functional reload
    MV3_SUBSCRIPTIONS: test

func:v3-default-subs:chromium:stable:incognito:
  extends: .functional
  variables:
    TEST_PARAMS: v3 chromium --testKinds functional reload --incognito
    MV3_SUBSCRIPTIONS: default

func:v3-default-subs:edge:linux:
  extends: .functional
  variables:
    TEST_PARAMS: v3 edge --testKinds functional reload
    MV3_SUBSCRIPTIONS: default

func:v2:edge:windows:
  extends: .edge:windows
  script:
    - npm test -- v2 edge --timeout=10000

func:v2:edge:windows:incognito:
  extends: .edge:windows
  script:
    - npm test -- v2 edge --incognito --timeout=10000

.fuzz:
  extends: .functional
  stage: fuzz
  script:
    - docker run --shm-size=512m -e TEST_PARAMS="v3 $BROWSER --testKinds fuzz --grep ^$FUNCTIONAL_AREA" functional
  parallel:
    matrix:
      - FUNCTIONAL_AREA:
        - "(Blocking|Pop-up\\sblocking)"
        - "(Element\\sHiding|Snippets)"
        - "API"
        - "(?!Blocking|Pop-up\\sblocking|Element\\sHiding|Snippets|API)"

fuzz:v3-default-subs:chromium:stable:
  extends: .fuzz
  variables:
    MV3_SUBSCRIPTIONS: default
    BROWSER: chromium
    
fuzz:v3-test-subs:chromium:stable:fuzz:
  extends: .fuzz
  variables:
    MV3_SUBSCRIPTIONS: test
    BROWSER: chromium

fuzz:v3-default-subs:chromium:stable:incognito:
  extends: .fuzz
  variables:
    MV3_SUBSCRIPTIONS: default
    BROWSER: chromium --incognito

fuzz:v3-default-subs:edge:linux:
  extends: .fuzz
  variables:
    MV3_SUBSCRIPTIONS: default
    BROWSER: edge

.integration:
  stage: integration
  extends: .docker-image
  variables:
    MANIFEST: mv2
  needs:
    - job: build:integration
      artifacts: true
  before_script:
    - apk add git
    - git clone https://gitlab.com/eyeo/adblockplus/abc/testpages.adblockplus.org.git
    - cp "extension/extension"$MANIFEST".zip" testpages.adblockplus.org
  script:
    - cd testpages.adblockplus.org &&
      docker build -t testpages --build-arg EXTENSION_FILE="extension"$MANIFEST".zip" . &&
      docker run --shm-size=512m -e SKIP_EXTENSION_DOWNLOAD="true" -e TESTS_EXCLUDE="Snippets$SKIP" -e BROWSER="$BROWSER" testpages
  after_script:
    - docker cp $(docker ps -aqf ancestor=testpages):/testpages.adblockplus.org/test/screenshots .
      2> /dev/null
  artifacts:
    paths:
      - screenshots/
    when: always
    expire_in: 1 mo

int:v2:chrome:latest:
  extends: .integration
  variables:
    BROWSER: chromium latest

int:v2:chrome:oldest:
  extends: .integration
  variables:
    BROWSER: chromium 77.0.3865.0

int:v2:chrome:beta:
  extends: .integration
  allow_failure: true
  variables:
    BROWSER: chromium beta

int:v2:firefox:latest:
  extends: .integration
  variables:
    BROWSER: firefox latest

int:v2:firefox:oldest:
  extends: .integration
  variables:
    BROWSER: firefox 68.0

int:v2:firefox:beta:
  extends: .integration
  variables:
    BROWSER: firefox beta

.integration:v3:
  extends: .integration
  variables:
    MANIFEST: mv3
    SKIP: "|Header|Sitekey|Subscriptions"

int:v3:chrome:latest:
  extends: .integration:v3
  allow_failure: true
  variables:
    BROWSER: chromium latest

int:v3:chrome:beta:
  extends: .integration:v3
  allow_failure: true
  variables:
    BROWSER: chromium beta

.pages:
  stage: pages
  dependencies:
    - build

include: ".gitlab-pages.yml"
