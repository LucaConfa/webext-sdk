import "mocha/mocha.js";
import "mocha/mocha.css";

import "./mocha/mocha-setup.js";
import "./expect-matchers.js";
import "./service-worker.js";
import "./request-filter.js";
import "./content-filter.js";
import "./api.js";
import "./popup-blocker.js";
import "./subscribe-links.js";
import "./allowlisting.js";

import {start} from "./mocha/mocha-runner.js";
start();
