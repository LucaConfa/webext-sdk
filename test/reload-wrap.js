import "mocha/mocha.js";
import "mocha/mocha.css";

import "./expect-matchers.js";
import "./mocha/mocha-setup-minimal.js";
import "./reload.js";

import {start} from "./mocha/mocha-runner.js";
start();
